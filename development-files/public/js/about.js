$('#boxClients').slick({
    slidesToShow: 4,
    slidesToScroll: 1,
    // prevArrow:"<button type='button' class='btn-prev'></button>",
    // nextArrow:"<button type='button' class='btn-next'></button>",
    dots: false,
    autoplay: true,
    autoplaySpeed: 2000,
    arrows: false,
    centerMode: true,
    centerPadding: '60px',
    responsive: [
        {
            breakpoint: 1024,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 1,
            }
        },
        {
            breakpoint: 480,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
            }
        },
    ]
});