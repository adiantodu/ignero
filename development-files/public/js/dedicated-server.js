$('.accord-itm').click((s)=>{
    $('.accord-body').slideUp()

    if( $(event.currentTarget).attr('class').search('active') == -1 ){
        $($(event.currentTarget).children()[1]).slideDown()
    }

    $(event.currentTarget).toggleClass('active')
})

$('#location_drop').click(()=>{
    $(event.currentTarget).toggleClass('active')
    $('#location_drop_body').slideToggle()
})

$('#location_drop_body > a').click(()=>{
  $('.title-drop').text($(event.currentTarget).text())
})


let product = [
  {
    'location':'Los Angeles',
    'data':[
        {'name':'Ryzen 5 5600X (Special)','price':'$99.00 USD','available':'126','setup':'10','cpu':'6 Cores/12 Threads @ 3.70GHz','ram':'64GB DDR4','storage':'500GB NVMe SSD','ipv4':'1 Included','ipv6':'On Demand','bandwidth':'20TB @ 1Gbps (Upgradable)','ipmi':'Yes','url':'https://my.ignero.com/index.php?m=liveinventory&pid=28&hash=ignero123894213','link':'https://my.ignero.com/store/ryzen-dedicated-servers/ryzen-5-5600x-special'},
        {'name':'Ryzen 7 5800X (Special)','price':'$149.00 USD','available':'0','setup':'10','cpu':'8 Cores/16 Threads @ 3.80GHz','ram':'128GB DDR4','storage':'1TB NVMe SSD','ipv4':'1 Included','ipv6':'On Demand','bandwidth':'20TB @ 1Gbps (Upgradable)','ipmi':'Yes','url':'https://my.ignero.com/index.php?m=liveinventory&pid=29&hash=ignero123894213','link':'https://my.ignero.com/store/ryzen-dedicated-servers/ryzen-7-5800x-special'},
        {'name':'Ryzen 9 5900X (Special)','price':'$199.00 USD','available':'126','setup':'10','cpu':'12 Cores/24 Threads @ 3.70GHz','ram':'128GB DDR4','storage':'2TB NVMe SSD','ipv4':'1 Included','ipv6':'On Demand','bandwidth':'20TB @ 1Gbps (Upgradable)','ipmi':'Yes','url':'https://my.ignero.com/index.php?m=liveinventory&pid=30&hash=ignero123894213','link':'https://my.ignero.com/store/ryzen-dedicated-servers/ryzen-9-5900x-special '}
    ]
  },
  {
    'location':'New York',
    'data':[
        {'name':'Ryzen 5 5600X (Special)','price':'$99.00 USD','available':'126','setup':'10','cpu':'6 Cores/12 Threads @ 3.70GHz','ram':'64GB DDR4','storage':'500GB NVMe SSD','ipv4':'1 Included','ipv6':'On Demand','bandwidth':'20TB @ 1Gbps (Upgradable)','ipmi':'Yes','url':'https://my.ignero.com/index.php?m=liveinventory&pid=28&hash=ignero123894213','link':'https://my.ignero.com/store/ryzen-dedicated-servers/ryzen-5-5600x-special'},
        {'name':'Ryzen 7 5800X (Special)','price':'$149.00 USD','available':'0','setup':'10','cpu':'8 Cores/16 Threads @ 3.80GHz','ram':'128GB DDR4','storage':'1TB NVMe SSD','ipv4':'1 Included','ipv6':'On Demand','bandwidth':'20TB @ 1Gbps (Upgradable)','ipmi':'Yes','url':'https://my.ignero.com/index.php?m=liveinventory&pid=29&hash=ignero123894213','link':'https://my.ignero.com/store/ryzen-dedicated-servers/ryzen-7-5800x-special'},
        {'name':'Ryzen 9 5900X (Special)','price':'$199.00 USD','available':'126','setup':'10','cpu':'12 Cores/24 Threads @ 3.70GHz','ram':'128GB DDR4','storage':'2TB NVMe SSD','ipv4':'1 Included','ipv6':'On Demand','bandwidth':'20TB @ 1Gbps (Upgradable)','ipmi':'Yes','url':'https://my.ignero.com/index.php?m=liveinventory&pid=30&hash=ignero123894213','link':'https://my.ignero.com/store/ryzen-dedicated-servers/ryzen-9-5900x-special '}
    ]
  },
]


function dedicatedData(){
  function tmplt(d){
      return `<div class="product-itm">
      <div class="top-block">
        <div class="txt-wrap"> 
          <div class="small">Name</div>
          <div class="big">${d.name}</div>
        </div>
        <div class="txt-wrap"> 
          <div class="small">Starting from</div>
          <div class="txt-inner-wrap">
            <div class="inner-big">${d.price}</div>
            <div class="inner-small">Monthly</div>
          </div>
        </div>
        <div class="txt-wrap"> 
        ${d.stockcontrol == 1 && d.qty == 0 ? '<div style="flex:1;"></div>' : `
            <div class="small">Availability</div>
            <div class="big">${d.stockcontrol == 1 ? d.qty : 'Infinite'}</div>
            `}
        </div>
        <div class="txt-wrap"> 
        ${d.stockcontrol == 1 && d.qty == 0 ? '<div style="flex:1;"></div>' : `
            <div class="small">Setup Time</div>
            <div class="txt-inner-wrap"><img src="images/clock.svg">
                <div class="inner-big"> 15</div>
                <div class="inner-small">Mins</div>
            </div>`}
        </div>
        <a href="${d.stockcontrol == 1 && d.qty == 0 ? '#' : d.link}" class="btn-primary ${d.stockcontrol == 1 && d.qty == 0 ? 'not-avail' : ''}"  disabled="${d.stockcontrol == 1 && d.qty == 0 ? 'true' : 'false'}">${d.stockcontrol == 1 && d.qty == 0 ? 'Out of Stock' : 'Order Now'}</a>
      </div>
      <div class="btm-block"> 
        <div class="txt-wrap"> 
          <div class="small">CPU</div>
          <div class="small">${d.cpu}</div>
        </div>
        <div class="txt-wrap"> 
          <div class="small">RAM</div>
          <div class="small">${d.ram}</div>
        </div>
        <div class="txt-wrap"> 
          <div class="small">Storage</div>
          <div class="small">${d.storage}</div>
        </div>
        <div class="txt-wrap"> 
          <div class="small">IPv4</div>
          <div class="small">${d.ipv4}</div>
        </div>
        <div class="txt-wrap"> 
          <div class="small">IPv6</div>
          <div class="small">${d.ipv6}</div>
        </div>
        <div class="txt-wrap"> 
          <div class="small">Bandwidth</div>
          <div class="small">${d.bandwidth}</div>
        </div>
        <div class="txt-wrap"> 
          <div class="small">IPMI &amp; Panel Access</div>
          <div class="small">${d.ipmi}</div>
        </div>
      </div>
    </div>`
  }

  var indexP = product.findIndex(x=>x.location == $('.title-drop').text())
  $('.product-itm-wrap').html('')
  $('.lds-ring').show()

  if(indexP > -1){
    product[indexP].data.forEach((s)=>{
        $.ajax({url: s.url, success: function(result){
            result = JSON.parse(result)
            s.qty = result.qty
            s.stockcontrol = result.stockcontrol
            $('.product-itm-wrap').append(tmplt(s))
            $('.lds-ring').hide()
        }})
    })
  }
}

function changeLocation(p){
  dedicatedData()
  $('.title-drop').text(p)
}

function locationData(){
  product.forEach((s,i)=>{
      if(i == 0){
        $('.title-drop').text(s.location)
      }
      $('#location_drop_body').append(`
        <a onclick="changeLocation('${s.location}')">${s.location}</a>
      `)
  })
}

$(document).ready(function(){
    
  dedicatedData()

  locationData()
    
});