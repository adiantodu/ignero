
$('#featuresHome').slick({
    responsive: [
        {
            breakpoint: 9999,
            settings: "unslick",
        },
        {
            breakpoint: 480,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                dots: true,
                arrows: false,
                infinite: false,
                // variableWidth: true,
                centerMode: true,
            }
        },
    ]
});