$('#mobilePricingCpanel').slick({
    responsive: [
        {
            breakpoint: 9999,
            settings: "unslick",
        },
        {
            breakpoint: 1024,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 1,
                dots: true,
                arrows: false,
                infinite: false,
                centerMode: false,
                centerPadding: '30px'
            }
        },
        {
            breakpoint: 480,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                dots: true,
                arrows: false,
                infinite: false,
                // variableWidth: true,
                centerMode: true,
                centerPadding: '30px'
            }
        },
    ]
});

$('#featuresPricing').slick({
    responsive: [
        {
            breakpoint: 9999,
            settings: "unslick",
        },
        {
            breakpoint: 480,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                dots: true,
                arrows: false,
                infinite: false,
                // variableWidth: true,
                centerMode: true,
            }
        },
    ]
});