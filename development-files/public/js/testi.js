$('.slide-wrapper').slick({
    slidesToShow: 3,
    slidesToScroll: 1,
    dots: false,
    autoplay: true,
    autoplaySpeed: 2000,
    arrows: false,
    variableWidth: true,
    responsive: [
        {
            breakpoint: 1028,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 1,
            }
        },
        {
            breakpoint: 480,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
            }
        },
    ]
});

$('.slickPrev').click(()=>{
    $('.slide-wrapper').slick('slickPrev')
})

$('.slickNext').click(()=>{
    $('.slide-wrapper').slick('slickNext')
})