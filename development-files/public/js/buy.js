$('.list-faq__header').on('click', function(){
    $('.list-faq').removeClass('is-active')
    $(this).parent().addClass('is-active')
})


$('.tab').on('click', function(){
    $('.tab').removeClass('btn-primary')
    $('.tab').addClass('btn-default')
    $(this).removeClass('btn-default')
    $(this).addClass('btn-primary')

    let tabid = $(this).attr("data-tab")
    $('.content-tab').removeClass('is-show')
    $('#' + tabid).addClass('is-show')
})

$('#boxClients').slick({
    slidesToShow: 4,
    slidesToScroll: 1,
    // prevArrow:"<button type='button' class='btn-prev'></button>",
    // nextArrow:"<button type='button' class='btn-next'></button>",
    dots: false,
    autoplay: true,
    autoplaySpeed: 2000,
    arrows: false,
    centerMode: true,
    centerPadding: '60px',
    responsive: [
        {
            breakpoint: 1024,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 1,
            }
        },
        {
            breakpoint: 480,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
            }
        },
    ]
});

$('#copy').on('click', function(){
    $('#code').select();
    document.execCommand('copy')
    $(this).text('COPIED')
    setTimeout(function() {
        $('#copy').text('COPY')
    },1000)
})